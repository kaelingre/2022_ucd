<!doctype html>

<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=1024" />
    <title>Gravitational Scattering at 4th Post-Minkowskian Order</title>
    
    <meta name="description" content="Presentation Slides made with impress.js" />
    <meta name="author" content="Gregor Kaelin" />

	<script type="text/x-mathjax-config">
	  MathJax.Hub.Config({
      "HTML-CSS": {
	  scale: 90,
	  styles: {
      '.MathJax_Display': {
      "margin": "20px 0"
      }
      }
	  },
	  TeX: { extensions: ["color.js"] },
	  });
	</script>
	<script type="text/javascript" src="extras/mathjax/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>

    <link href="css/main.css" rel="stylesheet" />
    <link rel="shortcut icon" href="favicon.png" />
    <link rel="apple-touch-icon" href="apple-touch-icon.png" />
  </head>

  <body class="impress-not-supported">

	<div class="fallback-message">
      <p>Your browser <b>doesn't support the features required</b> by impress.js, so you are presented with a simplified version of this presentation.</p>
      <p>For the best experience please use the latest <b>Chrome</b>, <b>Safari</b> or <b>Firefox</b> browser.</p>
	  <p>
		$$
		\definecolor{desyOrange}{RGB}{242,142,0}
		\DeclareMathOperator{\Arcsinh}{arcsinh}
		\DeclareMathOperator{\Arctan}{arctan}
		\DeclareMathOperator{\arcosh}{arcosh}
		\newcommand\dd{{\mathrm d}}
		\newcommand{\bb}{{\mathbf b}}
		\newcommand{\bk}{{\mathbf k}}
		\newcommand{\bl}{{\mathbf l}}
		\newcommand{\bm}{{\mathbf m}}
		\newcommand{\bn}{{\mathbf n}}
		\newcommand{\bp}{{\mathbf p}}
		\newcommand{\bq}{{\mathbf q}}
		\newcommand{\br}{{\mathbf r}}
		\newcommand{\bw}{{\mathbf w}}
		\newcommand{\bx}{{\mathbf x}}
		\newcommand{\by}{{\mathbf y}}
		\newcommand{\bz}{{\mathbf z}}
		\newcommand\cO{\mathcal{O}}
		\newcommand{\cD}{\mathcal{D}}
		\newcommand\cA{\mathcal{A}}
		\newcommand\cM{\mathcal{M}}
		\newcommand\cN{\mathcal{N}}
		\newcommand\cE{\mathcal{E}}
		\newcommand\cS{\mathcal{S}}
		\newcommand\cP{\mathcal{P}}
		\newcommand\cF{\mathcal{F}}
		\newcommand\cL{\mathcal{L}}
		\newcommand\cH{\mathcal{H}}
		\newcommand\Mp{M_{\rm Pl}}
		$$
	  </p>
	</div>

	<div id="impress">

	  <div id="title" class="step" data-x="0" data-y="0">
		<h1>Gravitational Scattering at 4th P<span class="orange">o</span>st-Minkowskian Order</h1>
		<p class="margin-bottom30">Work with Gihyuk Cho, Christoph Dlapa, Ryusuke Jinno, Zhengwen Liu, <br/>
		  Jakob Neef, Rafael Porto & Henrique Rubira<br/>
		  <a href="https://arxiv.org/abs/1910.03008">[1910.03008]</a>
		  <a href="https://arxiv.org/abs/1911.09130">[1911.09130]</a>
		  <a href="https://arxiv.org/abs/2006.01184">[2006.01184]</a>
		  <br/>
		  <a href="https://arxiv.org/abs/2007.04977">[2007.04977]</a>
		  <a href="https://arxiv.org/abs/2008.06047">[2008.06047]</a>
		  <a href="https://arxiv.org/abs/2106.08276">[2106.08276]</a>
		  <br/>
		  <a href="https://arxiv.org/abs/2112.03976">[2112.03976]</a>
		  <a href="https://arxiv.org/abs/2112.11296">[2112.11296]</a>
		  <a href="https://arxiv.org/abs/2207.00580">[2207.00580]</a>
		  <br/>
		  <a href="https://arxiv.org/abs/2209.01091">[2209.01091]</a>
		  <a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ">[2210.XXXXX]</a>
		</p>
		
		<img id="scatteringFig" src="figures/scattering.svg" alt="scattering">
		<img id="orbitFig" src="figures/orbit.svg" alt="orbit">
		
		<p id="author">
		  Gregor Kälin
		</p>
		
		<div class="margin-top50 margin-bottom30">
		  <img id="ercFig" src="figures/erc.svg" alt="erc">
		  <img id="desyFig" src="figures/logo_DESY.png" alt="desy">
		  <img id="euFig" src="figures/eu.svg" alt="eu">
		</div>
		
		<div>
		  <i>
			UCD 06.10.2022
		  </i>
		</div>
	  </div>

	  <div id="motivation" class="step" data-rel-x="1700" data-rel-to="title">
		<h2>M<span class="orange">o</span>tivation</h2>
		<p class="center">
		  <img id="waveformFig" src="figures/waveform.png" alt="waveform">
		</p>
	  </div>
	  
	  <div id="main" class="step" data-rel-x="1700" data-rel-to="motivation">
		<h2><span class="orange">O</span>verview</h2>
		<p class="center">
		  <img id="mainFig" src="figures/main.svg" alt="scattering">
		</p>
	  </div>

	  <div id="PNvsPM" class="step vanishPast" data-rel-x="0" data-y="-250" data-z="-1000" data-rel-to="main">
		<h1>Post-Newtonian vs<span style="color: #F28E00">.</span> Post-Minkowskian</h1>
		<p class="center">
		  PN expansion in \(v^2\sim \frac{Gm}{r} \ll 1\) vs. PM expansion in \(G\)<br/>
		  State-of-the-art for the complete \(\Delta p\) (or \(\chi\)) including radiation-reaction effects
		</p>
		<p class="center">
		  <table class="tablePNPM">
			<thead>
			  <tr><th></th> <th></th> <th scope="col" class="complete">0PN</th> <th scope="col" class="complete">1PN</th> <th scope="col" class="complete">2PN</th> <th scope="col" class="complete">3PN</th> <th scope="col" class="complete">4PN</th> <th scope="col">5PN</th> <th scope="col">6PN</th> </tr>
			</thead>
			<tbody>
			  <tr class="complete"><th scope="row">0PM</th> <td>$$~~1~~$$</td> <td>$$v^2$$</td> <td>$$v^4$$</td> <td>$$v^6$$</td> <td>$$v^8$$</td> <td>$$v^{10}$$</td> <td>$$v^{12}$$</td> <td>$$v^{14}$$</td></tr>
			  <tr class="complete"><th scope="row">1PM</th> <td/> <td>$$1/r$$</td> <td>$$v^2/r$$</td> <td>$$v^4/r$$</td> <td>$$v^6/r$$</td> <td>$$v^8/r$$</td> <td>$$v^{10}/r$$</td> <td>$$v^{12}/r$$</td></tr>
			  <tr class="complete"><th scope="row">2PM</th> <td/> <td/> <td>$$1/r^2$$</td> <td>$$v^2/r^2$$</td> <td>$$v^4/r^2$$</td> <td>$$v^6/r^2$$</td> <td>$$v^8/r^2$$</td> <td>$$v^{10}/r^2$$</td></tr>
			  <tr class="complete"><th scope="row">3PM</th> <td/> <td/> <td/> <td>$$1/r^3$$</td> <td>$$v^2/r^3$$</td> <td>$$v^4/r^3$$</td> <td>$$v^6/r^3$$</td> <td>$$v^8/r^3$$</td> <td class="comment">cons: Bern et al. [1901.04424]<br/>rad: Damour [2010.01641] </td></tr>
			  <tr class="complete"><th scope="row">4PM</th> <td/> <td/> <td/> <td/> <td>$$1/r^4$$</td> <td>$$v^2/r^4$$</td> <td>$$v^4/r^4$$</td> <td>$$v^6/r^4$$</td> <td class="comment">cons: Bern et al. [2112.10750]<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Dlapa et al. [2112.11296]<br/>rad: Dlapa et all [2210.XXXX]</td></tr>
			  <tr><th scope="row">5PM</th> <td class="complete"/> <td class="complete"/> <td class="complete"/> <td class="complete"/> <td class="complete"/> <td class="complete">$$1/r^5$$</td> <td>$$v^2/r^5$$</td> <td>$$v^4/r^5$$</td></tr>
			  <tr><th scope="row">6PM</th> <td class="complete"/> <td class="complete"/> <td class="complete"/> <td class="complete"/> <td class="complete"/> <td class="complete"/> <td>$$1/r^6$$</td> <td>$$v^2/r^7$$</td></tr>
			</tbody>
		  </table>
		</p>
	  </div>

	  <div id="pmeft" class="step" data-rel-x="1700" data-rel-to="main">
		<h2>A w<span class="orange">o</span>rldline EFT framework</h2>
		<a class="ref" href="https://arxiv.org/abs/2006.01184">[GK, Porto 2006.01184]</a>
		<ul>
		  <li>Purely <em>classical</em></li>
		  <li>Perturbative expansion in \(G\): particle physics/amplitudes toolbox</li>
		  <li>EFT methodology: Full action \(\rightarrow\) effective action \(\rightarrow\) deflection/fluxes/waveform/...</li>
		  <li>Complete: allows inclusion of radiation, finite size, spin, \(n\)-body</li>
		</ul>
		<h3><span class="h3span">Full the<span class="orange">o</span>ry</span></h3>
		<p class="center">
	      $$\begin{align}
		  S_{\rm EH} &= -2\Mp^2 \int \dd^4x \sqrt{-g} \, R[g]\\
		  S_{\rm pp} &= - \sum_a m_a \int \dd\sigma_a \sqrt{g_{\mu\nu}(x^\alpha_{a}(\sigma)) v_{a}^\mu(\sigma_a) v_{a}^\nu (\sigma_a)} + \dots\\
		  &\rightarrow -\sum_a \frac{m_a}{2} \int \dd\tau_a\,  g_{\mu\nu}(x_{a}(\tau_a)) v_{a}^\mu(\tau_a) v_{a}^\nu (\tau_a)+\dots\\
		  S_{\rm GF} + S_{\rm TD} &= \dots
		  \end{align}$$
		</p>
	  </div>

	  <div id="pmeft2" class="step" data-rel-x="0" data-rel-y="600" data-rel-to="pmeft">
		<h3><span class="h3span">Effective two-body acti<span class="orange">o</span>n</span></h3>
		<p class="centerLeft">
		  $$e^{i S_{\rm eff}[x_a] } = \int \cD h_{\mu\nu} \, e^{i S_{\rm EH}[h] + i S_{\rm GF}[h] + i S_{\rm TD} + i S_{\rm pp}[x_a,h]}$$
		</p>
		We optimized the EH-Lagrangian by cleverly chosing gauge-fixing terms and adding total derivatives:
		<ul>
		  <li>2-point Lagrangian: 2 terms</li>
		  <li>3-point Lagrangian: 6 terms</li>
		  <li>4-point Lagrangian: 18 terms</li>
		  <li>5-point Lagrangian: 36 terms</li>
		</ul>
		These numbers could even be further reduced by field redefinitions, but we don't want higher point WL-couplings. 
	  </div>

	  <div id="rad" class="step" data-rel-x="0" data-rel-y="730" data-rel-to="pmeft2">
		<h3><span class="h3span">Radiati<span class="orange">o</span>n reaction</span></h3>
		<a class="ref" href="https://arxiv.org/abs/2006.01184">[GK, Neef, Porto 2207.00580]</a>
		<ul class="margin-top60">
		  <li>For causal boundary conditions (\(i0\) prescription for graviton propagators) we need to use the in-in formalism \(\longrightarrow\) doubling of fields:
			$$\begin{align}
			\cS[h_1,h_2] = \cS_\textrm{EH}[h_1] - \cS_\textrm{EH}[h_2] -\sum_{A=1}^2 \frac{\kappa m_A}{2}\int\dd\tau_A &\left[h_{1,\mu\nu}(x_{1,A}(\tau_A))\dot{x}_{1,A}^\mu(\tau_A)\dot{x}_{1,A}^\nu(\tau_A)\right.\\
			&\quad\left.-h_{2,\mu\nu}(x_{2,A}(\tau_A))\dot{x}_{2,A}^\mu(\tau_A)\dot{x}_{2,A}^\nu(\tau_A)\right]
			\end{align}
			$$
		  </li>
		  <li>BUT: when computing the variation of the action it simplifies almost to usual in-out with the difference \(\textrm{Feynman}\rightarrow\textrm{retarded/advanced}\) propagator</li>
		  <div class="graphsRad">
			<div>
		  $$\left.\frac{\delta\cS_\textrm{eff}[x_+,x_-]}{\delta x_-^\alpha}\right|_{\substack{x_-\to 0\\x_+\to x}}^\textrm{1PM}=$$
		  </div>
		  <img id="graphsRad1Fig" src="figures/graphsRad1.svg" alt="graphsRad1"/>
    	<div class="marginLeft">
		  $$\left.\frac{\delta\cS_\textrm{eff}[x_+,x_-]}{\delta x_-^\alpha}\right|_{\substack{x_-\to 0\\x_+\to x}}^\textrm{2PM}=$$
		  </div>
		  <img id="graphsRad2Fig" src="figures/graphsRad2.svg" alt="graphsRad2"/>
		  </div>
		  <div class="graphsRad">
			<div>
		  $$\left.\frac{\delta\cS_\textrm{eff}[x_+,x_-]}{\delta x_-^\alpha}\right|_{\substack{x_-\to 0\\x_+\to x}}^\textrm{3PM}=$$
		  </div>
		  <img id="graphsRad3Fig" src="figures/graphsRad3.svg" alt="graphsRad3"/>
		  </div>
		</ul>
	  </div>

	  <div id="pmeft3" class="step" data-rel-x="0" data-rel-y="900" data-rel-to="rad">
		<h3><span class="h3span">PM deflecti<span class="orange">o</span>n</span></h3>
		In a Post-Minkowskian expansion:
		$$S_{\rm eff} = \sum_{n=0}^\infty \int \dd\tau_1\,G^n \cL_n [x_1(\tau_1),x_2(\tau_2)]$$
		with
		$$\cL_0 = - \frac{m_1}{2} \eta_{\mu\nu} v_1^\mu(\tau_1) v_1^\nu(\tau_1)$$
		E.o.m. from variation of the action
		$$-\eta^{\mu\nu}\frac{\dd}{\dd\tau_1} \left(\frac{\partial \cL_0}{\partial  v^\nu_1}\right) = m_1  \frac{\dd v_1^\mu}{\dd\tau_1}  = -\eta^{\mu\nu}\left(\sum_{n=1}^{\infty} \frac{\partial \cL_n}{\partial x^\nu_1(\tau_1)} - \frac{\dd}{\dd\tau_1} \left(\frac{\partial \cL_n}{\partial  v^\nu_1}\right)\right)$$
		allows us to compute the trajectories order by order:
		$$x^\mu_a(\tau_1) = b^\mu_a + u^\mu_a \tau_a + \sum_n G^n \delta^{(n)} x^\mu_a (\tau_a)$$
		with \(b=b_1-b_2\) the impact parameter and \(u_a\) the incoming velocty at infinity, fulfilling
		$$u_1\cdot u_2 = \gamma\,, \quad u_a\cdot b = 0\,.$$
	  </div>

	  <div id="pmeft4" class="step" data-rel-x="0" data-rel-y="650">
		<h3><span class="h3span">C<span class="orange">o</span>mputing observables</span></h3>
		Using above trajectories we can e.g. compute the deflection 
		$$\Delta p^\mu_1= m_1 \int_{-\infty}^{+\infty}\dd\tau \ddot{x}_1^\mu = - \eta^{\mu\nu} \sum_n \int_{-\infty}^{+\infty} \dd\tau {\partial \cL_n  \over\partial x^\nu_1}\,,$$
		or the change of angular momentum
		$$\Delta J^{\mu\nu}_1 = m_1 \int_{-\infty}^{+\infty} \dd\tau ( x_1^\mu \ddot{x}_1^\nu- \ddot{x}_1^\mu x_1^\nu)\,.$$
	  </div>

	  <div id="Dpres" class="step" data-rel-x="0" data-rel-y="650">
		<h3><span class="h3span">State-<span class="orange">o</span>f-the-art result at 4PM</span></h3>
		<a class="ref" href="https://www.youtube.com/watch?v=dQw4w9WgXcQ">[Dlapa, GK, Liu, Neef, Porto 2210.XXXXX]</a>
		<div class="center">
		  $$\Delta^{(n)} p_1^\mu =c^{(n)}_{1b}\, \hat b^\mu +  \sum_{a} c^{(n)}_{1\check{u}_a}\, \check{u}_a^\mu$$
		  <img id="res4PMFig" src="figures/res4PM.png" alt="res4PM"/>
		</div>
	  </div>

	  <div id="int1" class="step" data-rel-x="1700" data-rel-to="pmeft">
		<h2>Integrati<span class="orange">o</span>n</h2>
		Generic structure to any loop order (potential + radiation):
		$$
		\int \dd^Dq\frac{\delta(q\cdot u_1)\delta(q\cdot u_2)e^{i b\cdot q}}{(q^2)^m}
		\underbrace{\int \dd^D\ell_1\cdots\dd^D\ell_L\frac{\delta(\ell_1\cdot u_{a_1})\cdots\delta(\ell_L\cdot u_{a_L})}{(\ell_1\cdot u_{b_1}\pm i0)^{i_1}\cdots(\ell_L\cdot u_{b_L}\pm i0)^{i_L}(\textrm{sq. props})}}_{\textrm{Cut Feynman integrals with linear and square propagators}}
		$$
		<ul>
		  <li>Single scale integrals in \(\gamma = u_1\cdot u_2\)</li>
		  <li>We can use all modern integration methods from the amplitudes community</li>
		  <li>One delta function per loop \(\rightarrow\) half of the linear WL propagators are cut</li>
		  <li>@3PM: One single family + \(i0^+\) prescription for linear propagators. We have solved the DEQs for all \(\sim20\) master integrals (including those appearing for dissipative and spin effects).</li>
		  <li>@4PM: Only two families of square propagators + \(i0^+\) prescription for linear/retarded propagators.</li>
		</ul>
	  </div>

	  <div id="int2" class="step" data-rel-y="750" data-rel-x="0">
		<h3><span class="h3span">Integration pr<span class="orange">o</span>cedure</span></h3>
		<ul>
		  <li>Tensor reduction: e.g. standard PaVe</li>
		  <li>Integration-By-Part (IBP) reduction: FIRE6 + LiteRed</li>
		  <ul>
			<li>Retarded propagators break symmetries: 1094 Master Integrals (MIs)</li>
		  </ul>
		  <li>Method of differential equations: bring into \(\epsilon\)-form <a class="refInline">[Kotikov, Remiddi, Henn]</a></li>
		  <ul>
			<li>Precanonical form is sufficient for blocks containing elliptic integrals. The \(\cO(\epsilon^0)\) DEQ can be solved for diagonal elements: 3x3 elliptic blocks \(\rightarrow\) 3rd order differential equation which is solved by a quadratic expression in terms of complete elliptic integrals. Off-diagonal integrals can then be solved order by order in \(\epsilon\).</li>
		  </ul>
		  <li>Method of regions to compute the boundary constants <a class="refInline">[Beneke, Smirnov]</a></li>
		  <ul>
			<li>Use symmetries and relations among BCs to reduce their number</li>
			<li>1&2PM: potential region only</li>
			<li>3PM: pot + \(\textrm{rad}^1\)</li>
			<li>4PM: pot + \(\textrm{rad}^1\) + \(\textrm{rad}^2\)</li>
			<li>For retarded props this works better in momentum space: Integration splits into rad and pot loop(s)</li>
		  </ul>
        </ul>
	  </div>

	  <div id="int3" class="step" data-rel-y="700" data-rel-x="0">
		<h3><span class="h3span">Numerical integrati<span class="orange">o</span>n with machine learning</span></h3>
		<a class="ref" href="https://arxiv.org/abs/2209.01091">[Jinno, GK, Liu, Rubira 2209.01091]</a>
		<ul>
		  <li>Why numerical integration?</li>
		  <ul>
			<li>Cross-checks are important and incredibly useful</li>
			<li>Might be the only available method at higher loops</li>
			<li>Analytical bootstrap</li>
		  </ul>
		  <li>Need fast algorithms for high precision!</li>
		  <li>Our idea: teach a neural network to do the Monte-Carlo integration making use of the <em>normalizing flows</em> technology.</li>
		</ul>		
	  </div>

	  <div id="int4" class="step" data-rel-y="550" data-rel-x="0">
		<ul>
		  <li><em>Importance Sampling:</em> Pick points for Monte-Carlo evaluation such that regions of large integrand \(|f|\) gain more weight.</li>
		  <li>I.e. take a distribution \(x(G)\), \(\dd G = g(x) \dd x\)
			$$I = \int_\Omega \dd x f(x) = \int_{\tilde\Omega} \dd G \frac{f(x(G))}{g(x(G))}$$
			that minimizes the variance
			$$\sigma^2=\frac{1}{N-1}\left[\frac{1}{N}\sum_i \left(\frac{f(G_i)}{g(G_i)}\right)^2-\left(\frac{1}{N}\sum_i \frac{f(G_i)}{g(G_i)}\right)^2\right]$$
		  </li>
		  <li>Hence, optimally \(g(x) = f(x)/I\), but it should also be invertible and fast to evaluate.</li>
		</ul>
	  </div>

	  <div id="int5" class="step" data-rel-y="650" data-rel-x="0">
		<ul>
		  <li>Traditional Monte-Carlo algorithms assume \(g(x_1,\dots,x_n) = g(x_1) \dots g(x_n)\) and then use an \(N\)-dimensional histogram for each dimension.</li>
		  <ul>
			<li>Often not true at all and leads to a bad sampling.</li>
		  </ul>
		  <li>Better: Machine learning in the form of a neural network setup</li>
		  <div class="center">
			<img id="NNFig" src="figures/NN.png" alt="NN"/>
			$$J =\left|\begin{pmatrix} 1 & 0 \\ \frac{\partial C}{\partial m}\frac{\partial m}{\partial x_A} & \frac{\partial C}{\partial x_B}\end{pmatrix}\right| = \left| \frac{\partial C}{\partial x_B} \right|$$
		  </div>
		</ul>
	  </div>
	  
	   <div id="int6" class="step" data-rel-y="800" data-rel-x="0">
		 <ul>
		   <li>Promising results for (potential) boundary integrals up to 4-loops</li>
		   <div class="center">
			 <img id="NNplots1Fig" src="figures/NNplots1.png" alt="NN1"/>
			 <img id="NNplots2Fig" src="figures/NNplots2.png" alt="NN2"/>
		   </div>
		 </ul>
	   </div>
	   

	  <div id="B2B" class="step" data-rel-x="1700" data-rel-y="0" data-rel-to="int1">
		<h2>Boundary-T<span class="orange">o</span>-Bound (B2B) Dictionary</h2>
		<a class="ref" href="http://arxiv.org/abs/arXiv:1911.09130">[1911.09130, w/ Porto]</a>
		<a class="ref" href="http://arxiv.org/abs/arXiv:1910.03008">[1910.03008, w/ Porto]</a>
		<br/>
		Conservative motion described by a Hamiltonian:
		$$H(\bp,r) = E \quad \Longrightarrow \quad \bp(r,E)$$
		<div class="center">
		  <img id="scatteringFig" src="figures/scattering.svg" alt="scattering"/>
		  vs.
		  <img id="orbitFig" src="figures/orbit.svg" alt="orbit"/>
		</div>
		<ul>
		  <li>Scattering: \(r_\textrm{min}=\tilde{r}_-\) is the positive real root of \(p_r\): \(p_r^2(r,E)=\bp^2(r,E)-(p_\infty b)^2/r^2\) for positive binding energy.</li>
		  <li>Bound: \(r_\pm\) are positive real roots of \(p_r\): \(p_r^2(r,E)=\bp^2(r,E)-J^2/r^2\) for negative binding energy.</li>
		</ul>
		Using \(J=p_\infty b\), we have shown that the radii are related by an analytic continuation 
		$$\begin{align*}
		r_-(J) &= r_{\textrm{min}}(b)\\
		r_+(J) &= r_-(-J) = r_{\textrm{min}}(-b)
		\end{align*}$$
		with \(b\in i\mathbb{R}\) for bound kinematics \(p_\infty^2\leq0\).
	  </div>
	  
	  <div id="B2B2" class="step" data-rel-x="0" data-rel-y="720">
		<h3><span class="h3span">Angle to <span class="orange">o</span>rbital Elements: Firsov</span></h3>
		<a class="ref" href="https://arxiv.org/abs/1911.09130">[1911.09130, w/ Porto]</a>
		<a class="ref" href="https://arxiv.org/abs/1910.03008">[1910.03008, w/ Porto]</a>
		<br/>
		The scattering angle contains all the information.
		$$r_{\textrm{min}} \overset{\textrm{Firsov}}{=}  b \exp\left[ -\frac{1}{\pi} \int_{b}^\infty \frac{\chi(\tilde b,E)\dd\tilde b}{\sqrt{\tilde b^2-b^2}}\right]
		=b \prod_{n=1}^\infty e^{-\frac{(GM)^n\chi_b^{(n)}(E)\Gamma\left(\frac{n}{2}\right)}{b^n\sqrt{\pi}\Gamma\left(\frac{n+1}{2}\right)}}$$
		with 
		$$\frac{\chi}{2} = \sum_{n=1} \chi^{(n)}_b \left(\frac{GM}{b}\right)^n $$		
	  </div>
	  
	  <div id="B2B3" class="step" data-rel-x="0" data-rel-y="580">
		<h3><span class="h3span">Angle to periastr<span class="orange">o</span>n advance</span></h3>
		<a class="ref" href="http://arxiv.org/abs/arXiv:1911.09130">[1911.09130, w/ Porto]</a>
		<a class="ref" href="http://arxiv.org/abs/arXiv:1910.03008">[1910.03008, w/ Porto]</a>
		Scattering angle:
		$$\chi(b,E) +\pi = 2b \int_{r_{\rm min}}^\infty \frac{\dd r}{r\sqrt{r^2\bar\bp^2(r,E)-b^2}}$$
		with 
		<br/>
		Periastron advance:
		$$\Delta \Phi + 2\pi =  2J \int_{r_-}^{r_+} \frac{\dd r}{r\sqrt{r^2\bp^2(r,E)-J^2}}$$
		Don't forget: \(J=p_\infty b\), \(\bar\bp = \bp/p_\infty\).
	  </div>
	  
	  <div id="B2B4" class="step" data-rel-x="0" data-rel-y="0">
		<span id="rP" class="vanishPast">\(\textcolor{desyOrange}{\tiny r_\textrm{min}(-J)}\)</span>
		<span id="rM" class="vanishPast">\(\textcolor{desyOrange}{\tiny r_\textrm{min}(J)}\)</span>
		<span id="intPeriastron" class="vanishPast">
		  $$\begin{align}
		  &=2J \int_{r_\textrm{min}(J)}^{\infty} \frac{\dd r}{r\sqrt{r^2\bp^2(r,E)-J^2}}\\
		  &\quad-2J \int_{r_\textrm{min}(-J)}^{\infty} \frac{\dd r}{r\sqrt{r^2\bp^2(r,E)-J^2}}\\
		  &=\chi(J,E)+\chi(-J,E)+2\pi
		  \end{align}$$
		  </span>
	  </div>

	  <div id="B2B5" class="step" data-rel-x="0" data-rel-y="790">
		<h3><span class="h3span">Angle to radial acti<span class="orange">o</span>n</span></h3>
		<a class="ref" href="http://arxiv.org/abs/arXiv:1911.09130">[1911.09130, w/ Porto]</a>
		<a class="ref" href="http://arxiv.org/abs/arXiv:1910.03008">[1910.03008, w/ Porto]</a>
		<br/>
		We can get the radial action from the angle by integrating
		$$\chi(J,E) = \frac{\dd}{\dd J} \cS_r^\text{hyp}(J,E)$$
		The radial action for hyperbolic and elliptic motion are themselves related by a similar analytic continuation
		$$(G M \mu) i_r^\text{hyp}(J,E) = \cS_r^\text{hyp}(J,E) = \frac{2}{2 \pi}\int_{r_\text{min}(J,E)}^\infty p_r(J,E,r)\dd r$$
		$$(G M \mu) i_r^\text{ell}(J,E) = \cS_r^\text{ell}(J,E) = \frac{2}{\pi}\int_{r_-(J,E)}^{r_+(J,E)} p_r(J,E,r)\dd r$$
		$$\Rightarrow i_r^\text{ell}(J)=i_r^\text{hyp}(J)- i_r^\text{hyp}(-J)$$
		In PM expanded form:
		$$
		i_r^\text{ell}(j,\cE) \equiv \frac{{\cal S}_r}{GM\mu} =  {\rm sg}(\hat p_\infty)\chi^{(1)}_j(\cE) - j \left(1 + \frac{2}{\pi} \sum_{n=1}  \frac{\chi^{(2n)}_j({\cE})}{(1-2n)j^{2n}}\right)
		$$
	  </div>

	  <div id="B2B6" class="step" data-rel-x="0" data-rel-y="790">
		<h3><span class="h3span">Radiati<span class="orange">o</span>n</span></h3>
		<a class="ref" href="https://arxiv.org/abs/2112.03976">[2112.03976, w/ Cho, Porto]</a>
		<p class="margin-top60">
		In a similar way we can analytically continue radiative observables, e.g. the energy and angular momentum flux
		$$\begin{align}
		\Delta E_\textrm{ell}(J,\cE) &= \Delta E_\textrm{hyp}(J,\cE)-\Delta E_\textrm{hyp}(J,\cE)\\
		\Delta J_\textrm{ell}(J,\cE) &= \Delta J_\textrm{hyp}(J,\cE)+\Delta J_\textrm{hyp}(J,\cE)\\
		\end{align}$$
		</p>
	  </div>
	  

	  <div id="alignSpin" class="step" data-rel-x="0" data-rel-y="680">
		<h3><span class="h3span">Analytic continuation for aligned spins<span class="orange">.</span></span></h3>
		<a class="ref" href="http://arxiv.org/abs/arXiv:1911.09130">[1911.09130, w/ Porto]</a>
		Works for aligned spin. Motion is still in a plane!
		<div class="center">
		<div class="centerBox">
		  $$\frac{\chi(J,\cE)+\chi(-J,\cE)}{2\pi} = \frac{\Delta\Phi(J,\cE)}{2\pi}$$
		  <p class="center small">
			where \(J\) is now the <em>total</em> the total angular momentum, i.e. orbital angular momentum + spins.
		  </p>
		</div>
		</div>
		<ul>
		  <li>Explicit checks for known PN and PM results work neatly!</li>
		  <li>Relies on the invariance of the (canonical) radial momentum \(p_r\) under \(J\rightarrow -J\), which is true for a quasi-isotropic gauge (given to us automatically by the amplitudes construction).</li>
		</ul>
	  </div>

	  <div id="Firsov1" class="step" data-rel-x="1700" data-rel-y="0" data-rel-to="B2B">
		<h2>PM to PN: The Magic of Firsov</h2>
		<img id="firsovFig" src="figures/Firsov.jpg" alt="firsov">
		Let's consider a simple example
		$${\Delta \Phi}(j,\cE)= \sum_{n=1} \Delta \Phi_j^{(2n)}(\cE)/j^{2n}$$
		with \(j=J/(G M \mu)\). We just established
		$$\Delta \Phi_j^{(2n)}(\cE)= 4\, \chi^{(2n)}_j(\cE)$$
		Let's assume we don't know anything about \(\chi^{(4)}_j\). Does this mean we don't know anything about \(\Delta \Phi_j^{(4)}\)? There's lot of lower PN information that our PM results up to \(\chi_j^{(3)}\) should contain. Where are they?
	  </div>
	  
	  <div id="Firsov2" class="step" data-rel-x="0" data-rel-y="680">
		<h3><span class="h3span">Firs<span class="orange">o</span>v's formula</span></h3>
		Let us invert
		$$\chi(b,E) = -\pi + 2b \int_{r_{\rm min}}^\infty \frac{\dd r}{r\sqrt{r^2\bar\bp^2(r,E)-b^2}} = \sum_{n=1} \chi^{(n)}_b(E) \left(\frac{GM}{b}\right)^n $$
		<a class="inlineRef" href="">[Firsov '53]</a>: dependence on \(r_\textrm{min}\) drops out
		<div class="center"><div class="centerBox">
			$$\bar\bp^2(r,E) = \exp\left[ \frac{2}{\pi} \int_{r|\bar{\bp}(r,E)|}^\infty \frac{\chi(\tilde b,E)\dd\tilde b}{\sqrt{\tilde b^2-r^2\bar\bp^2(r,E)}}\right] = 1 + \sum_{n=1}^\infty f_n(E) \left(\frac{GM}{r}\right)^n$$
		</div></div>
		These integrals are easy to perform in a PM-expanded form and one finds:
		$$\chi_b^{(n)} = \frac{\sqrt{\pi}}{2} \Gamma\left(\frac{n+1}{2}\right)\sum_{\sigma\in\mathcal{P}(n)}\frac{1}{\Gamma\left(1+\frac{n}{2} -\Sigma^\ell\right)}\prod_{\ell} \frac{f_{\sigma_{\ell}}^{\sigma^{\ell}}}{\sigma^{\ell}!}$$
		The inversion thereof also exists.
	  </div>

	  <div id="Firsov3" class="step" data-rel-x="0" data-rel-y="650">
		<h3><span class="h3span">Back to our pr<span class="orange">o</span>blem</span></h3>
		$$\frac{1}{4}\Delta \Phi_j^{(4)}= \chi^{(4)}_j = \left(\frac{p_\infty}{\mu}\right)^4\chi_b^{(4)}=\left(\frac{p_\infty}{\mu}\right)^4\frac{3\pi}{16}(2f_1f_3+f_2^2+2f_4)$$
		and in turn
		$$\begin{align}
		f_1&=2\chi_b^{(1)}\\
		f_2&=\frac{4}{\pi}\chi_b^{(2)}\\
		f_3&=\frac{1}{3}\left(\chi_b^{(1)}\right)^3+\frac{4}{\pi}\chi_b^{(1)}\chi_b^{(2)}+\chi_b^{(3)}
		\end{align}$$
		Prefectly reproduces 1 and 2PN information at order \(j^{-4}\).
	  </div>
	  
	  <div id="conclusions" class="step" data-rel-x="1700" data-rel-y="0" data-rel-to="Firsov1">
		<h2>Conclusi<span class="orange">o</span>ns</h2>
		<ul>
		  <li>PMEFT+B2B: systematic and efficient framework to study the classical gravitational 2-body dynamics.</li>
		  <li>Modern integration techniques can handle all the integrals we have found.</li>
		  <li>We have the complete answer for the deflection up to 4PM level.</li>
		  <li>Firsov's formula is bridging \(\chi\leftrightarrow \bp^2 \leftrightarrow \cH\) and \(\text{PN}\leftrightarrow\text{PM}\).
		</ul>
		
	  </div>

	  <div id="outlook" class="step" data-rel-x="1700" data-rel-y="0">
		<h2>Outlo<span class="orange">o</span>k</h2>
		<ul>
		  <li>We have not reached any tool's maximal capacity, but 5PM seems clos the limit.</li>
		  <li>Integration tools need to be improved for retarded propagators.</li>
		  <li>Iterated elliptic integrals need to be understood better.</li>
		  <li>B2B for arbitrary spin and non-local contributions.</li>
		</ul>
	  </div>

	  <div id="final" class="step" data-rel-x="0" data-rel-y="660">
		<p class="center">
		  <img id="mainFig" src="figures/main.svg" alt="scattering">
		</p>
		<p class="small">
		  This research is supported by the ERC-CoG “Precision Gravity: From the LHC to LISA” provided by the European Research Council (ERC) under the European Union’s H2020 research and innovation programme (grant No. 817791), by the DFG under Germany’s Excellence Strategy ‘Quantum Universe’ (No. 390833306).
		</p>
	  </div>
	  
  	</div>

	<div id="impress-toolbar"></div>

	<!--
	<div class="hint">
      <p>Use a spacebar or arrow keys to navigate. <br/>
		Press 'P' to launch speaker console.</p>
	</div>
	-->
	<script>
	  if ("ontouchstart" in document.documentElement) { 
		  document.querySelector(".hint").innerHTML = "<p>Swipe left or right to navigate</p>";
	  }
	  </script>

	<script src="js/impress.js"></script>
	<script>impress().init();</script>

  </body>
</html>
