LATEX=pdflatex
LATEXOPT=--shell-escape
NONSTOP=--interaction=nonstopmode

LATEXMK=latexmk
LATEXMKOPT=-pdf

all: main.pdf scattering.pdf integration.pdf graphsEFT.pdf orbit.pdf res4PM.pdf
	pdf2svg main.pdf main.svg
	pdf2svg scattering.pdf scattering.svg
	pdf2svg integration.pdf integration.svg
	pdf2svg graphsEFT.pdf graphsEFT.svg
	pdf2svg orbit.pdf orbit.svg
	pdf2svg res4PM.pdf res4PM.svg
	cp *.svg ../public/figures/

main.pdf: main.tex gTikZBeamer.tex
	$(LATEXMK) $(LATEXMKOPT) -pdflatex="$(LATEX) $(LATEXOPT) $(NONSTOP) %O %S" main

scattering.pdf: scattering.tex gTikZ.tex
	$(LATEXMK) $(LATEXMKOPT) -pdflatex="$(LATEX) $(LATEXOPT) $(NONSTOP) %O %S" scattering

integration.pdf: integration.tex gTikZ.tex
	$(LATEXMK) $(LATEXMKOPT) -pdflatex="$(LATEX) $(LATEXOPT) $(NONSTOP) %O %S" integration

graphsEFT.pdf: graphsEFT.tex gTikZBeamer.tex
	$(LATEXMK) $(LATEXMKOPT) -pdflatex="$(LATEX) $(LATEXOPT) $(NONSTOP) %O %S" graphsEFT

orbit.pdf: orbit.tex gTikZBeamer.tex
	$(LATEXMK) $(LATEXMKOPT) -pdflatex="$(LATEX) $(LATEXOPT) $(NONSTOP) %O %S" orbit

res4PM.pdf: res4PM.tex gTikZBeamer.tex
	$(LATEXMK) $(LATEXMKOPT) -pdflatex="$(LATEX) $(LATEXOPT) $(NONSTOP) %O %S" res4PM

clean:
	rm -f *~ *.log *.aux *.svg *.auxlock *.fls *latexmk

.PHONY: all
